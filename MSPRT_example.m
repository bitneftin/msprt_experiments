
% Exercise on MSPR

%  #######################################################
%  #  _______   __  _____ ______ _______ _ ____   _____  #
%  #         \ |  |   ___|   ___|__   __| |    \ |       #
%  #          \|  |  __| |  __|    | |  | |     \|       #
%  #       |\     | |____| |       | |  | |   |\         #
%  #  _____| \____| _____|_|       |_|  |_|___| \______  #
%  #                                                     #
%  #######################################################

% Coded by Giammarco Valenti
% /// KEEP IT SIMPLE! \\\

% The MSPRT totally crazy

addpath(genpath('..'));

% Script to try the SPRT algorithm

p2 =  5;
p1 =  15;

sigma1 = 5;
sigma2 = 5;

case1 = p1  + sigma1*randn(1,100); % iid with 5 and 5 sigma
case2 = p2  + sigma2*randn(1,100);

x = [ case1(1:20) case2(1:30) case1(21:80) case2(31:100) case1(81:100) ];

%x = case2;

S   = zeros(1,length(x));
LLH = zeros(1,length(x));



for i = 2:length(x)
    S(i) = S(i-1) + log(evid(x(i),p2,sigma2)/evid(x(i),p1,sigma1)); %log-likelihood ratio: gaussiana noto x1 ma variato p
    LLH(i)  = evid(x(i),p2,sigma2);
end

figure(1)
plot([ S ]')
title('SPRT')
xlabel('sample')
ylabel('salience')



%% trying to implement the MSPRT

threshold = 0.6;
%Distanza tra le medie
delta_mi = 1;
%Fattore moltiplicativo della varianza
si_rel = 1;

mi = [ 0:delta_mi:delta_mi*3 ];
si = ([ 1 1 1 1 ].*delta_mi*si_rel).^2;

caseGen = [ 100 100 100 100 ; 1 2 3 4 ]; % lengths and cases!

cases = [ones(1,100)*caseGen(2,1), ones(1,100)*caseGen(2,2), ones(1,100)*caseGen(2,3), ones(1,100)*caseGen(2,4)];

x = [];

for i = 1:size(caseGen,2)
    x = [ x ( mi(caseGen(2,i))+si(caseGen(2,i)).*randn(1,caseGen(1,i)) ) ];
end

c = 0;
Test = {};
P = {};

g = 1.0;
S_race = zeros(length(mi),length(x));
Y     = zeros(length(mi),length(x)); % see Gurney

biasone = [];
biasino = [];
ev = [];

x_aug = x.*ones( length(mi) , length(x) ); % augment x to use parallel

%% Forgetting factor
S_race = zeros(length(mi),length(x));
Y     = zeros(length(mi),length(x)); % see Gurney
vai = 0;
for H = 0:1:50
    vai = vai + 1;
    ind = [];
    for i = 2:length(x)
        S_race(:,i) = S_race(:,i-1) + evid( x_aug(:,i) , mi , si )'; %log-likelihood ratio: gaussiana noto x1 ma variato p

        ev(:,i) = evid( x_aug(:,i) , mi , si )';

        Y(:,i) = Y(:,i-1) + ev(:,i);

        L(:,i) = g*Y(:,i) - log(sum(g*exp(Y(:,i))));


    %     if any(i == cumsum(caseGen(1,:))) % multiple of cases length
    %         Y(:,i)     = zeros(size(mi));
    %         S_race(:,i) = zeros(size(mi));
    %     end

        if any(exp(L(:,i)) >= threshold) % multiple of cases length
            ind_aux = find(exp(L(:,i))>=threshold);
            if cases(i) == ind_aux
                ind = [ind , ind_aux];
            end 

            f = min(i-1,H);
            Y(:,i)      = sum(ev(:,i-f:i),2);
            S_race(:,i) = zeros(size(mi));
        end
    end
    Test{vai}.ind = ind;
    Test{vai}.val = H;
    % Sum of all, is it one?
    P{vai}    = exp(L); % Probabilities (L is log(P))
    %Ptot      = sum(P,1);
end

    
figure(6);
t=[];
for xx = 1:50
    for i = [1,2,3,4]
        t(i,xx) = sum(Test{xx}.ind == i);
    end
end
bar(t','stacked');
axis([-inf inf 0 400]);

%% Percentage storage
S_race = zeros(length(mi),length(x));
Y     = zeros(length(mi),length(x)); % see Gurney
vai = 0;
for H = 0:0.01:1
    vai = vai + 1;
    ind = [];
    for i = 2:length(x)
        S_race(:,i) = S_race(:,i-1) + evid( x_aug(:,i) , mi , si )'; %log-likelihood ratio: gaussiana noto x1 ma variato p

        ev(:,i) = evid( x_aug(:,i) , mi , si )';

        Y(:,i) = Y(:,i-1) + ev(:,i);

        L(:,i) = g*Y(:,i) - log(sum(g*exp(Y(:,i))));


    %     if any(i == cumsum(caseGen(1,:))) % multiple of cases length
    %         Y(:,i)     = zeros(size(mi));
    %         S_race(:,i) = zeros(size(mi));
    %     end

        if any(exp(L(:,i)) >= threshold) % multiple of cases length
            ind_aux = find(exp(L(:,i))>=threshold);
            if cases(i) == ind_aux
                ind = [ind , ind_aux];
            end 
            Y(:,i)     = Y(:,i)*H;
            S_race(:,i) = zeros(size(mi));
        end
    end
    Test{vai}.ind = ind;
    Test{vai}.val = H;
    % Sum of all, is it one?
    P{vai}    = exp(L); % Probabilities (L is log(P))
    %Ptot      = sum(P,1);
end

    
figure(5);
t=[];
for xx = 1:100
    for i = [1,2,3,4]
        t(i,xx) = sum(Test{xx}.ind == i);
    end
end
bar(t','stacked');
axis([-inf inf 0 400]);
%%
vai = 20;
Test{vai}.val

figure(2)
h = plot([ P{vai}; threshold.*ones(1,length(P{vai})) ]','Marker','x');%
%h = plot([ P; Y ]','Marker','x');%threshold.*ones(1,length(P))
set(h(length(mi)+1),'Color',[1 1 1].*0.3);
set(h(length(mi)+1),'Marker','none');
% h = plot([ S_race; P; Ptot ]','Marker','x');
% for i = 1:length(mi)
%     set(h( i+length(mi) ),'Color',h(i).Color); % color simmetry
%     set(h(2*length(mi)+1),'Color',[1 1 1].*0.3);
%     h(i).Color(4) = 0.3;
% end
set(h,'Linewidth',2);
title('MSPRT')
xlabel('sample')
ylabel('salience')
%vline( cumsum(caseGen(1,:)),'black')
legend({'1(race)','2','3','4','1(MSPRT)','2','3','4'})
grid minor;
%axis([-inf inf 0 2])

%% Motor cortex trial

mi = 0.5;
si = 0.05;

jerko     = mi + si*randn(1,600);

channels = [ 2.5 2 1.8 1.6 1.4 1.2 1 0.75 0.6 0.5 0.4 0.3 0.2 0.1 ];
channels = [ -channels 0 fliplr(channels) ]'; % create the channels for the motorcortex

jerko_aug = jerko.*ones( length(channels) , length(jerko) ); % augment x to use parallel

NC = length(channels);

gains = 2 * hanning(NC);

threeshold = 1000;

motorcortex_history      = zeros(NC,length(jerko));
motorcortex_history_MSPRT = zeros(NC,length(jerko));

for i = 1:size(jerko,2)
    
    motorcortex = evid( jerko_aug(:,i) , channels , si ); % gaussian likelihood
    
    motorcortex_history(:,i) = motorcortex;
    
    mc = mean(motorcortex_history(:,1:i),2);

    motorcortex_history_MSPRT(:,i) = - gains .* mc + log( sum( exp ( gains .* mc  ) ) );

end

figure(3)
title('motorcortex likelihood')

h = plot(motorcortex_history_MSPRT');
h(20).LineWidth = 2;
legend(num2str(channels))


ylabel('likelihood')
xlabel('samples')
grid on;



%% functions


function auss = evid( x , mi , si )
    auss = (1/2*pi)^(1/2); % go to infinite if sigma 0
    % GO DEEPER
    if size(mi) ~= size(x)
        x = x';
        if size(mi) ~= size(x)
            error('recidivo negli input farlocchi')
        end
    end
    % auss = 1; % trying to normalize, instead of the area
    auss = (auss./si).*exp( ( -0.5 ) .*( (x - mi) ./ (si) ).^2 );
end






















