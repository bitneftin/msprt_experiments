%% 
close all;  clc; clear;

%% data
plotflag = 0;

% sim params
n_channels = 100;
sigma = 0;

% MSPRT parameters
timeWindowSize  = 5;
gain            = 1;
threshold       = 0.01;
forgetting_factor = 0.0;

%% compare performance with number of channels - no noise
n_channel_vec = int16(linspace(2,100,10));

evid_n_channels = [];

for i=1:length(n_channel_vec)
    out = sim_MSPRT(n_channel_vec(i), sigma, timeWindowSize, gain, threshold, forgetting_factor, 0);
    %% keep track evid
    evid_n_channels = [evid_n_channels, out(1)];
end

% plot out
close all
figure(1), grid on
plot(n_channel_vec, evid_n_channels,'Color',[0. 0.4470 0.7410],'LineWidth',1.2)
xlabel('Number of channels','Interpreter','latex','FontSize',12),
ylabel('Maximum evidence','Interpreter','latex','FontSize',12)

%% compare performance with WTA - noise
sigma_vec = (linspace(0,0.25,10));

gain = 1;
threshold       = 0.1;
forgetting_factor = 0.8;

perf_sigma_hist = [];
perf_sigma_hist_WTA = [];

for i=1:length(sigma_vec)
    out = sim_MSPRT(10, sigma_vec(i), timeWindowSize, gain, threshold, forgetting_factor, 0);
    %% keep track evid
    perf_sigma_hist = [perf_sigma_hist, out(2)];
    perf_sigma_hist_WTA = [perf_sigma_hist_WTA, out(3)];
end

%
close all
figure(4),  hold on

plot(sigma_vec, perf_sigma_hist,'Color',[0. 0.4470 0.7410],'LineWidth',1.2)
plot(sigma_vec, perf_sigma_hist_WTA,'Color',[0.6350 0.0780 0.1840],'LineWidth',1.2)

xlabel('$\sigma$','Interpreter','latex','FontSize',12),
ylabel('Number of errors','Interpreter','latex','FontSize',12)
%axis([0 sigma_vec(end) 0 1000])
% yyaxis right
% set(gca,'YColor','k')
% plot(sigma_vec,(1000-perf_sigma_hist)/1000,'--','Color','b','LineWidth',0.8);
% plot(sigma_vec,(1000-perf_sigma_hist_WTA)/1000,'--','Color','r','LineWidth',0.8);
% ylabel('Guess percentage','Interpreter','latex','FontSize',12,'Color',[0.4940 0.1840 0.0])
% axis([0 sigma_vec(end) 0 1])
legend('MSPRT','WTA')


%% simulate
function out_MSPRT = sim_MSPRT(n_channels, sigma, timeWindowSize, gain, threshold, forgetting_factor, plotflag)

% set seed for repeatibility
rng(10)

% action
n_change = 10;
n_sample_change = 100;


% parametrize number of channels
data_len = n_sample_change*n_change;

evidHist = zeros(1,data_len);    % maximum evidence history
reset    = zeros(1,data_len);    % reset history
win_channel = zeros(1,data_len);
win_channel_WTA = zeros(1,data_len);
mc = zeros(n_channels,timeWindowSize);

mean_vec = randn(1,n_channels);
signals = zeros(n_channels, data_len);
signals_clean = signals;

for i=1:n_channels
    for j=1:n_change
        signals_clean(i,((j-1)*n_sample_change+1):j*n_sample_change)=rand;%randi(100);
        signals(i,((j-1)*n_sample_change+1):j*n_sample_change)=signals_clean(i,j*n_sample_change)+sigma*randn(1,n_sample_change);
    end
end

% obtain groundtruth
[~, win_channel_true] = max(signals_clean,[],1);

%% simulation
for i = 1:data_len
    
    j = mod(i-1,timeWindowSize)+1;
    
    % append signals
    mc(:,j)=signals(:,i);
    
    % compute mean in proper direction
    mean_mc = mean(mc,2);
    
    % compute evidence
    logLik = (gain.*mean_mc - log(sum(exp(gain.*mean_mc))));
    
    % get maximum evidence
    [maxEvid, max_channel] = max(exp(logLik));
    
    % accumulate evidence
    evidHist(i) = maxEvid;
    
    [~, win_channel_WTA(i)] = max(mc(:,j));
    
    if maxEvid > threshold
        % take decision
        mc(:,j)  = forgetting_factor*mean_mc;
        reset(i) = 1;
        win_channel(i) = max_channel;
    else
        % follow previous decision if any
        if i>1
            win_channel(i) = win_channel(i-1);
        end
    end    
   
end

%% post processing
n_error = sum(int16(boolean(win_channel-win_channel_true)));
n_error_WTA = sum(int16(boolean(win_channel_WTA-win_channel_true)));

disp(['number of reset:   ', num2str(sum(reset))]);
disp(['maximum evidence:  ', num2str(max(evidHist))]);
disp(['number of errors:      ', num2str(n_error)]);
disp(['number of errors WTA:  ', num2str(n_error_WTA)]);


if plotflag
    close all
    
    % plot singals
    figure(1)
    hold on
    
    for i=1:n_channels
        txt = ['X = ',num2str(i)];
        % plot(signals_clean(i,:),'DisplayName',txt)
        plot(signals(i,:),'DisplayName',txt)
    end
    
    hold off
    legend show

    % plot choices
    figure(2), hold on, grid on
    plot(win_channel,'*')
    plot(win_channel_WTA,'+','MarkerSize',1)
    plot(win_channel_true,'o','MarkerSize',2)
    legend('MSPRT','WTA','true')
    
    % plot evidence
    figure(3), grid on
    plot(evidHist)
    title('History of evidence')

    
end

%% return
out_MSPRT = [max(evidHist), n_error, n_error_WTA];

end



